#!/usr/bin/env python3
import gzip

xenbase_version = "XB2021_12"
filename_fa = 'xtropMRNA_NCBI.%s.fasta.gz' % xenbase_version

f_fa = gzip.open(filename_fa, 'rt')
for line in f_fa:
    if line.startswith('>'):
        tmp_tokens = line.strip().split('|')

        tmp_h = '%s|gi=%s' % (tmp_tokens[3], tmp_tokens[1])
        print(">%s" % tmp_h)
    else:
        tmp_line = line.strip().upper()
        if len(tmp_line) > 0:
            print(tmp_line)
f_fa.close()

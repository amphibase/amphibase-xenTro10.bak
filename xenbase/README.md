# Verion 2021_12

# Input files
- xtropMRNA_XenBase.XB2021_12.fasta.gz : http://ftp.xenbase.org/pub/Genomics/Sequences/xtropMRNA.fasta
- xtropMRNA_NCBI.XB2021_12.fasta.gz : http://ftp.xenbase.org/pub/Genomics/Sequences/NCBI/xtropMRNA.fasta
- XENTR_10.0_Xenbase.transcripts.fa.gz : http://ftp.xenbase.org/pub/Genomics/JGI/Xentr10.0/sequences/XENTR_10.0_Xenbase.transcripts.fa
- XENTR_9.1_Xenbase.transcripts.fa.gz : http://ftp.xenbase.org/pub/Genomics/JGI/Xentr9.1/sequences/XENTR_9.1_Xenbase.transcripts.fa

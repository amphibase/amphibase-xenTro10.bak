#!/usr/bin/env python3
import gzip
import sys

xenbase_version = 'XB2021_12'

filename_fa = 'xtropMRNA_XenBase.%s.fasta.gz' % xenbase_version

filename_out = 'XENTR_tx.%s.clean.fa' % xenbase_version
filename_NCAA = 'XENTR_tx.%s.NCAA.fa' % xenbase_version

seq_list = dict()
exclude_list = dict()
f_fa = gzip.open(filename_fa, 'rt')
for line in f_fa:
    if line.startswith('>'):
        tmp_h_tokens = line.strip().split()
        tmp_tokens = tmp_h_tokens[0].lstrip('>').split('|')
        gi = tmp_tokens[1]
        acc_id = tmp_tokens[3]

        tmp_h = 'NoName|%s|gene_id=NA|gi=%s' % (acc_id, gi)
        if line.find('GenePageIDs:') >= 0:
            tmp2 = line.strip().split('GenePageIDs:')[1].split()[0]
            tmp_tokens2 = tmp2.split('|')
            tmp_name = tmp_tokens2[0]
            if len(tmp_tokens2) > 1:
                gene_id = tmp_tokens2[1]
            else:
                gene_id = 'NA'

            if tmp_name.find('provisional:') >= 0:
                tmp_name2 = tmp_name.split('provisional:')[1].rstrip(']')
                tmp_name = '%s;%s' % (tmp_name2, tmp_name.split()[0])

            tmp_name = tmp_name.split()[0]
            tmp_h = '%s|%s|gene_id=%s|gi=%s' % (tmp_name, acc_id, gene_id, gi)

        seq_list[tmp_h] = []
    else:
        tmp_line = line.strip().upper()
        if len(tmp_line) > 0:
            seq_list[tmp_h].append(tmp_line)

        extra_N_list = list(set(tmp_line) - set(['A', 'T', 'G', 'C', 'N']))
        if len(extra_N_list) != 0:
            exclude_list[tmp_h] = 1
            sys.stderr.write('NCAA: %s %s\n' % (tmp_h, ''.join(extra_N_list)))
f_fa.close()

f_out = open(filename_out, 'w')
f_NCAA = open(filename_NCAA, 'w')
for tmp_h, tmp_seq_list in seq_list.items():
    if tmp_h in exclude_list:
        f_NCAA.write((">%s\n%s\n" % (tmp_h, ''.join(tmp_seq_list))))
    else:
        f_out.write((">%s\n%s\n" % (tmp_h, ''.join(tmp_seq_list))))
f_out.close()
f_NCAA.close()
